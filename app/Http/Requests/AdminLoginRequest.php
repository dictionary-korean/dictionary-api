<?php

namespace App\Http\Requests;

class AdminLoginRequest extends BaseRequest
{
   /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_name' => ['required'],
            'password' => ['required'],
        ];
    }

    public function messages()
    {
        return [
            'user_name.required'=> __("[user_name] is required"),
            'password.required'=> __("[password] is required"),
        ];
    }

}
