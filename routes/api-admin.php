<?php

use App\Http\Controllers\Admin\AuthController;
use App\Http\Controllers\Admin\UploadController;
use Illuminate\Support\Facades\Route;


Route::prefix('admin')->group(function () {

    Route::post('/auth/login', [AuthController::class, 'login']);

    Route::middleware('auth:sanctum')->group(function () {
        Route::post('upload',[UploadController::class, 'upload']);
        Route::post('ck-upload',[UploadController::class, 'ckUpload']);
        Route::post('/me', [AuthController::class, 'me']);
    });

});