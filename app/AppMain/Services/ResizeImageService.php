<?php

namespace App\AppMain\Services;

use App\AppMain\Config\AppConst;
use Illuminate\Support\Facades\Storage;
use Imagick;

class ResizeImageService
{
    /**
     * Upload thumb resize
     * @param 
     */
    public function resize($filePath, $width = AppConst::THUMB_CONFIG_WIDTH, $height = AppConst::THUMB_CONFIG_HEIGHT)
    {
        $fullFilePath = public_path($filePath);
        if(file_exists($fullFilePath)){
            $image = new Imagick($fullFilePath);
            $imageWidth = $image->getImageWidth();
            $fileName = basename($filePath);
            $fileNameThumb = AppConst::PREFIX_THUMB_IMAGE . $fileName;
            if($imageWidth < $width){
                $width = $imageWidth;
            }
            $filePathThumb = str_replace($fileName, $fileNameThumb, $filePath);
            $image->scaleImage($width, (int) $height);
            $image->stripImage();
            clearstatcache(dirname(public_path($fullFilePath)));
            if(!empty($filePathThumb)){
                $this->makeDirectory($filePathThumb);
            }
            $result = $image->writeImage(public_path($filePathThumb));

            if($result){
                return [
                    'thumb_path' => $filePathThumb,
                    'thumb_url' => url($filePathThumb)
                ];
            }
        }
        return [];
    }

    /**
     * Upload thumb resize use Imagick
     * @param 
     */
    public function resizeByExtension($filePath, $width = AppConst::THUMB_CONFIG_WIDTH, $height = AppConst::THUMB_CONFIG_HEIGHT, $ext = AppConst::EXTENSION_CONFIG_RESIZE)
    {
        $fullFilePath = public_path($filePath);
        if(file_exists($fullFilePath)){
            $image = new Imagick($fullFilePath);
            $imageWidth = $image->getImageWidth();
            $fileName = basename($filePath);
            $fileNameThumb = AppConst::PREFIX_THUMB_IMAGE . $fileName;
            if($imageWidth < $width){
                $width = $imageWidth;
            }
            $filePathThumb = str_replace($fileName, $fileNameThumb, $filePath);
            $filePathThumb = str_replace(AppConst::UPLOAD_CONFIG_PATH, AppConst::UPLOAD_CONFIG_RESIZE_PATH, $filePathThumb);
            $image->scaleImage($width, (int) $height);
            $image->stripImage();
            clearstatcache(dirname(public_path($fullFilePath)));
            $extCurrent = pathinfo($fullFilePath, PATHINFO_EXTENSION);
            $filePathThumb = str_replace($extCurrent, $ext, $filePathThumb);
            if(!empty($filePathThumb)){
                $this->makeDirectory($filePathThumb);
            }
            $result = $image->writeImage($ext . ':' . public_path($filePathThumb));
            if($result){
                return [
                    'thumb_path' => $filePathThumb,
                    'thumb_url' => url($filePathThumb)
                ];
            }
        }
        return [];
    }

    private function makeDirectory($filePath){
        $configs = config('filesystems.disks');
        $dirname = str_replace(AppConst::UPLOAD_CONFIG_PATH, '/',dirname($filePath));
        foreach($configs as $key => $config){
            if(empty($config['root'])) continue;
            if(strpos(public_path($filePath), $config['root']) !== false){
                Storage::disk($key)->makeDirectory($dirname);
                break;
            }
            
        }
    }
}