<?php

namespace App\Http\Controllers\Deploy;

use App\Http\Controllers\Controller;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class DeployController extends Controller
{
    public function index($type) {
        $command = "sh /var/www/TheGioiBep/";
        if($type == "api") {
            $command = $command . "TheGioiBepService/deploy.sh";
        } else if ($type == "admin") {
            $command = $command . "TheGioiBepAdmin/deploy.sh";
        } else if ($type == "web") {
            $command = $command . "TheGioiBepFrontend/deploy.sh";
        } else {
            dd("Command invalid");
        }

        $result = $this->runProcess($command);
        dd($result);
    }

    public function runProcess($command) {
        $process = Process::fromShellCommandline($command);
        $process->run();

        // executes after the command finishes
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        return $process->getOutput();
    }
}
