<?php

namespace App\AppMain\Services;

use App\AppMain\Config\AppConst;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Str;

class FileUploadService
{
    /**
     * Upload photos
     * @param 
     */
    public function uploadPhotos($files, $folderPath, $uploadConfigName = AppConst::UPLOAD_CONFIG_NAME)
    {
        $basePath = Config::get('filesystems.disks.' . $uploadConfigName . '.base_path');
        $arrFileUploaded = [];

        foreach ($files as $file) {
            $fullFolderPath = $basePath . $folderPath;

            // Create folder if not exist
            //Storage::makeDirectory(public_path($fullFolderPath));

            // Save photo to disk
            $originFileName = urlencode($file->getClientOriginalName());
            $milliseconds = round(microtime(true) * 1000);
            $ext = pathinfo($originFileName, PATHINFO_EXTENSION);
            $name = pathinfo($originFileName, PATHINFO_FILENAME);

            $fileName = Str::slug($name) . "_" . $milliseconds . ".$ext";
            $file->storeAs($folderPath, $fileName, $uploadConfigName);

            $fullFilePath = $fullFolderPath . "/" . $fileName;

            // Get image size
            $dataSize = getimagesize(public_path($fullFilePath));
            $imageWidth = 0;
            $imageHeight = 0;
            if($dataSize){
                list($imageWidth, $imageHeight) = $dataSize;
            }

            $arrFileUploaded[] = [
                'fileName' => $fileName,
                'path' => $fullFilePath,
                'url' => url($fullFilePath),
                'image_width' => $imageWidth,
                'image_height' => $imageHeight
            ];
        }

        return $arrFileUploaded;
    }

}
