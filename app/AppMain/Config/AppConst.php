<?php

namespace App\AppMain\Config;

class AppConst
{
    const UPLOAD_CONFIG_NAME = "upload";
    const UPLOAD_CONFIG_PATH = "/uploads/";
    const UPLOAD_TYPE_CKEDITOR = 1;
    const UPLOAD_FOLDER_TYPE = [
        self::UPLOAD_TYPE_CKEDITOR => "ckeditor"
    ];
    const UPLOAD_CONFIG_RESIZE_PATH = self::UPLOAD_CONFIG_PATH . "resize/";
    const EXTENSION_CONFIG_RESIZE = "webp";
    const PREFIX_THUMB_IMAGE = "thumb_";
    const THUMB_CONFIG_WIDTH = 200;
    const THUMB_CONFIG_HEIGHT = null;
    const THUMB_CONFIG_QUALITY = 75;
}
