<?php

namespace App\Console\Commands;

use App\Models\Admin;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;

class GenerateAdminAccount extends Command
{
    // php artisan generate_admin_account admin 123456 admin@gmail.com

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate_admin_account {username} {password} {email}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $username = $this->argument('username');
        $password = $this->argument('password');
        $email = $this->argument('email');

        $user = new Admin();
        $user->user_name = $username;
        $user->password = Hash::make($password);
        $user->email = $email;
        $user->save();
        return 0;
    }
}