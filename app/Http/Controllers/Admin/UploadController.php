<?php

namespace App\Http\Controllers\Admin;

use App\AppMain\Config\AppConst;
use App\AppMain\Services\FileUploadService;
use App\AppMain\Services\MediaService;
use App\AppMain\Services\ResizeImageService;
use App\Http\Controllers\Controller;
use App\Http\Requests\UploadFormRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Throwable;

use function App\AppMain\Helpers\responseJsonFail;
use function App\AppMain\Helpers\responseJsonSuccess;

class UploadController extends Controller
{
    protected $uploadService;
    protected $mediaService;
    protected $resizeImageService;
    public function __construct(
        FileUploadService $uploadService,
        MediaService $mediaService,
        ResizeImageService $resizeImageService
    ) 
    { 
        $this->uploadService = $uploadService; 
        $this->mediaService = $mediaService; 
        $this->resizeImageService = $resizeImageService; 
    }

    public function upload(UploadFormRequest $request) {
        DB::beginTransaction();
        try{
            $file = $request->file("upload");
            $type = $request->type;
            $arrFileUploaded = $this->uploadService->uploadPhotos([$file], $type);
            if($arrFileUploaded){
                foreach($arrFileUploaded as $key => $fileUploaded){
                    $arrThumbUploaded = $this->resizeImageService->resize($fileUploaded['path']);
                    $this->resizeImageService->resizeByExtension($fileUploaded['path']);
                    $arrFileUploaded[$key] =  array_merge($arrThumbUploaded, $arrFileUploaded[$key]);
                    $media_id = $this->mediaService->createMedia([
                        'url' => $fileUploaded['path'],
                        'type' => array_search($type, AppConst::UPLOAD_FOLDER_TYPE)
                    ]);
                    $arrFileUploaded[$key]['media_id'] = $media_id;
                }
            }
            DB::commit();
        } catch (Throwable $e) {
            DB::rollBack();
            Log::warning($e->getMessage());
            return responseJsonFail(false);
        }

        return responseJsonSuccess($arrFileUploaded);
    }

    public function ckUpload(Request $request) {
        $result = [ "error" => __("Upload error") ];
        try{
            if (!$request->hasFile("upload")) {
                $result = [ "error" => __("Upload file is require") ];
            }else{
                $file = $request->file("upload");
                $type = AppConst::UPLOAD_FOLDER_TYPE[AppConst::UPLOAD_TYPE_CKEDITOR];
                $arrFileUploaded = $this->uploadService->uploadPhotos([$file], $type);
                if(!empty($arrFileUploaded[0])){
                    $result = [
                        "uploaded" => (int) true, 
                        "fileName" => $arrFileUploaded[0]['fileName'] ?? '', 
                        "url" => $arrFileUploaded[0]['url'] ?? ''
                    ];
                }
            }
            
        } catch (Throwable $e) {
            Log::warning($e->getMessage());
        }
        return $result;
    }
}
