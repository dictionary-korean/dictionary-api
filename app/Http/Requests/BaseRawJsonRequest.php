<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Validator;

class BaseRawJsonRequest extends BaseRequest
{
    public function validator(){
        // Validate if Json first
        $content = $this->instance()->getContent();

        $rules = [
            'content' => ['json']
        ];

        $message = [
            'content.json' => __("Request data must be valid JSON format")
        ];

        $validator = Validator::make(['content' => $content], $rules, $message);
        
        if($validator->fails()) {
            return $validator;
        }

        // If json
        $data = json_decode($content, true);
        return Validator::make($data, $this->rules(), $this->messages());
    }
}
