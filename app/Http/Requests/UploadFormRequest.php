<?php

namespace App\Http\Requests;

use App\AppMain\Config\AppConst;
use Illuminate\Validation\Rule;

class UploadFormRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => ['required', Rule::in(AppConst::UPLOAD_FOLDER_TYPE)],
            'upload' => ['required', 'image', 'mimes:jpeg,png,jpg,gif', 'max:2048'],
        ];
    }

    public function messages()
    {
        $listType = implode(", " ,AppConst::UPLOAD_FOLDER_TYPE);
        return [
            'type.in'=> __("Type in") . $listType
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'type' => $this->type ?? AppConst::UPLOAD_FOLDER_TYPE[AppConst::UPLOAD_TYPE_CKEDITOR]
        ]);
    }
}
