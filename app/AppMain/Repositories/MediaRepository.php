<?php

namespace App\AppMain\Repositories;

use App\Models\Media;

class MediaRepository extends BaseRepository
{
    public function getModel()
    {
        return Media::class;
    }


    public function insertGetId(array $data)
    {
        //reset model
        $this->getModel();
        return $this->model->insertGetId($data);
    }

}
