<?php

namespace App\AppMain\Services;

use Illuminate\Support\Facades\Hash;

use App\AppMain\Repositories\AdminRepository;

use function App\AppMain\Helpers\responseJsonFail;

use const App\AppMain\Helpers\HTTP_CODE_SUCCESS;
use const App\AppMain\Helpers\HTTP_CODE_UNAUTHORIZED;
use const App\AppMain\Helpers\RESPONSE_STATUS_SUCCESS;

class AuthService
{
    protected $adminRepository;
    public function __construct(
        AdminRepository $adminRepository
    ) 
    {
        $this->adminRepository = $adminRepository;
    }

    public function login($userName, $password) {
        $user = $this->adminRepository->findOne('user_name', $userName);

        if(empty($user)) {
            return responseJsonFail(__('User does not exist'), HTTP_CODE_UNAUTHORIZED);
        }

        if(!Hash::check($password, $user->password)) {
            return responseJsonFail(__('Password incorrect'), HTTP_CODE_UNAUTHORIZED);
        }

        $token = $user->createToken('auth_token')->plainTextToken;

        return response(
            [
                'status' => RESPONSE_STATUS_SUCCESS,
                'data' => $user,
                'access_token' => $token,
                'token_type' => 'Bearer'
            ]
        , HTTP_CODE_SUCCESS);
    }
}
