<?php

namespace App\AppMain\Services;

use App\AppMain\Repositories\MediaRepository;

class MediaService
{
    protected $repository;
    public function __construct(
        MediaRepository $repository
    )
    {
        $this->repository = $repository;
    }

    /**
     * Upload photos
     * @param 
     */
    public function createMedia($data)
    {
        $mediaId = $this->repository->insertGetId($data);
        return $mediaId;
    }

}
