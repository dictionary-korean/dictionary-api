<?php

namespace App\Http\Controllers\Admin;

use App\AppMain\Services\AuthService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\AdminLoginRequest;

class AuthController extends Controller
{
    protected $authService;

    public function __construct(
        AuthService $authService
    ) 
    {
        $this->authService = $authService;
    }

    public function login(AdminLoginRequest $request)
    {
        $userName = $request->user_name;
        $password = $request->password;
        return $this->authService->login($userName, $password);
    }

    public function me(Request $request)
    {
        return $request->user();
    }
}
