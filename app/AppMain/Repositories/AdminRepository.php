<?php

namespace App\AppMain\Repositories;

use App\Models\Admin;

/**
 *
 */
class AdminRepository extends BaseRepository
{
    public function getModel()
    {
        return Admin::class;
    }

    public function create($attibutes = [])
    {
        return parent::create($attibutes);
    }
}
