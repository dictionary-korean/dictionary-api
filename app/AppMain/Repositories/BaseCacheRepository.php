<?php

namespace App\AppMain\Repositories;

use Illuminate\Support\Facades\Redis;

class BaseCacheRepository
{
    public function saveData($key, $value) {
        Redis::set($key, $value);
    }

    public function getData($key, $default="") {
        return Redis::get($key);
    } 

    public function removeData($key) {
        return Redis::del($key);
    }
}
