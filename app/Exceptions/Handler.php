<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;

use const App\AppMain\Helpers\HTTP_CODE_UNAUTHORIZED;

use function App\AppMain\Helpers\responseJsonFail;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });

        $this->renderable(function (\Illuminate\Auth\AuthenticationException $e, $request) {
            return responseJsonFail(__("Not authenticated"), HTTP_CODE_UNAUTHORIZED);
            // if ($request->is('api/*')) {
            //     return response()->json([
            //         'status'  => 0,
            //         'errors' => 'Not authenticated',
            //     ], 401);
            // }
        });
    }
}
