<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Media extends Model
{
    use HasFactory, SoftDeletes;
    protected $table = 'medias';
    protected $fillable = [
        'type',
        'url'
    ];
    protected $appends = ['path'];
    protected $hidden = ['pivot'];

    public function getPathAttribute() {
        if(!empty($this->url)) {
            return url($this->url);
        } else {
            return null;
        }
    }
}
